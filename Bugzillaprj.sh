curl -X POST \
  http://10.100.0.111:8080/rest/api/2/project \
  -H 'Authorization: Basic c2hpdmFwcmFzYWQuaDpwcmFzYWQxNDMmJg==' \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
 "key": "SHIV1",
 "name": "SHIV1 Sample",
 "projectTypeKey": "software",
 "projectTemplateKey": "com.pyxis.greenhopper.jira:gh-scrum-template",
 "description": "Example Project description",
 "lead": "Shivaprasad.h",
   "permissionScheme": 12404,
 "assigneeType": "PROJECT_LEAD"
 }'

curl -X POST \
  http://10.102.5.166:8080/rest/api/2/project \
  -H 'Authorization: Basic c2hpdmFwcmFzYWQuaDpwcmFzYWQxNDMmJg==' \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
-d '{
"key": "SHIVMMMM",
 "name": "SHIVMMMMM Sample",
 "projectTypeKey": "software",
 "projectTemplateKey": "com.pyxis.greenhopper.jira:gh-scrum-template",
 "description": "Example Project description",
 "lead": "Shivaprasad.h",
   "permissionScheme": 12404,
 "assigneeType": "PROJECT_LEAD"
 }'
